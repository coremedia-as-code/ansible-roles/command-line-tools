import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/tmp/deployment_artefacts",
    "/etc/ansible/facts.d",
    "/opt/coremedia/",
    "/opt/coremedia/content-management-server-tools",
    "/opt/coremedia/content-management-server-tools/bin",
    "/opt/coremedia/content-management-server-tools/properties/corem",
    "/var/log/coremedia/content-management-server-tools",
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/etc/ansible/facts.d/content-management-server-tools.fact",
    "/opt/coremedia/content-management-server-tools/bin/cm",
    "/opt/coremedia/content-management-server-tools/properties/corem/capclient.properties",
    "/opt/coremedia/content-management-server-tools/properties/corem/publisher.properties",
    "/opt/coremedia/content-management-server-tools/properties/corem/sql.properties"
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file


def test_user(host):
    assert host.group("coremedia").exists
    assert host.user("coremedia").exists

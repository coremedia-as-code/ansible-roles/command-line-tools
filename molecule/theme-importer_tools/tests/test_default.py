import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/tmp/deployment_artefacts",
    "/etc/ansible/facts.d",
    "/opt/coremedia/",
    "/opt/coremedia/theme-importer-tools",
    "/opt/coremedia/theme-importer-tools/bin",
    "/opt/coremedia/theme-importer-inbox",
    "/opt/coremedia/theme-importer-tools/properties/corem",
    "/var/log/coremedia/theme-importer-tools",
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/etc/ansible/facts.d/theme-importer-tools.fact",
    "/opt/coremedia/theme-importer-tools/bin/cm",
    "/opt/coremedia/theme-importer-tools/properties/corem/theme-importer.properties"
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file


def test_user(host):
    assert host.group("coremedia").exists
    assert host.user("coremedia").exists

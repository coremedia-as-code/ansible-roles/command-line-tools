import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/tmp/deployment_artefacts",
    "/etc/ansible/facts.d",
    "/opt/coremedia/",
    "/opt/coremedia/workflow-server-tools",
    "/opt/coremedia/workflow-server-tools/bin",
    "/opt/coremedia/workflow-server-tools/properties/corem",
    "/var/log/coremedia/workflow-server-tools",
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/etc/ansible/facts.d/workflow-server-tools.fact",
    "/opt/coremedia/workflow-server-tools/bin/cm",
    "/opt/coremedia/workflow-server-tools/properties/corem/capclient.properties",
    "/opt/coremedia/workflow-server-tools/properties/corem/sql.properties",
    "/opt/coremedia/workflow-server-tools/properties/corem/workflowclient.properties",
    "/opt/coremedia/workflow-server-tools/properties/corem/workflowserver.properties"
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file


def test_user(host):
    assert host.group("coremedia").exists
    assert host.user("coremedia").exists
